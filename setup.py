from setuptools import setup
exec(open('helloapp/_version.py').read())


setup(
    name="helloapp",
    author="Shuki",
    author_email="asdasd@asdas.com",
    description="A nice app for greeting :)",
    version=__version__,
    packages=["helloapp"],
    package_data={'helloapp': ['*']},
    install_requires=[
        'Flask'
    ]
)

