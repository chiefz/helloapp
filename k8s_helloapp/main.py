from helloapp import app

if __name__ == '__main__':
    try:
        print("Hello App initiated")
        app.run(debug=True, use_reloader=False, host='127.0.0.1')
    except Exception as e:
        print("GlobalError")
