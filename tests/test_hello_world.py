# test_hello.py
import unittest
from helloapp.hello import app


class TestHelloWorld(unittest.TestCase):
    def test_hello(self):
        response = app.test_client().get('/')
        assert response.status_code == 200
        assert response.data == b'Hefllo, World!'

    def test_greet(self):
        response = app.test_client().get('/greet')

        assert response.status_code == 200
        assert response.data == b'greetings'